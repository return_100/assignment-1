package net.therap.service;

import net.therap.model.LogParserModel;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author al.imran
 * @since 21/03/2021
 */
public class LogParserService {
    private static final int UNSET_VALUE = -1;
    private static final int TOTAL_HOURS = 24;
    private static final String TIME_REGEX = "(\\d){2}:(\\d){2}:(\\d){2},(\\d){3}";
    private static final String GET_REQUEST_REGEX = "G,|";
    private static final String POST_REQUEST_REGEX = "P,";
    private static final String URI_REGEX = "URI=[^,]+";
    private static final String RESPONSE_TIME_REGEX = "time=(\\d)+ms";
    private static final String GET_REQUEST = "G,";
    private static final String POST_REQUEST = "P,";
    private static final String TIME_DELIMETER = ":";
    private static final String RESPONSE_TIME_DELIMETER = "=|(ms)";
    private final LogParserModel[] logData = new LogParserModel[TOTAL_HOURS];

    public LogParserService() {
        for (int i = 0; i < (TOTAL_HOURS); ++i) {
            this.logData[i] = new LogParserModel();
        }
    }

    public void readLogParserData(String fileName) {
        File file = new File(fileName);
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        while (scanner != null && scanner.hasNext()) {
            this.extractData(scanner.nextLine());
        }
    }

    private void extractData(String data) {
        int getOrPost = extractGetOrPost(data);
        if (getOrPost == UNSET_VALUE) {
            return;
        }
        int hourValue = extractHourValue(data);
        String uri = extractUri(data);
        int responseTime = extractResponseTime(data);
        if (getOrPost == 1) {
            logData[hourValue].addTotalGetRequest();
        } else {
            logData[hourValue].addTotalPostRequest();
        }
        logData[hourValue].addUri(uri);
        logData[hourValue].addTotalResponseTime(responseTime);
    }

    private int extractHourValue(String data) {
        Pattern pattern = Pattern.compile(TIME_REGEX);
        Matcher matcher = pattern.matcher(data);
        String time = "";
        while (matcher.find()) {
            time = matcher.group();
        }
        String[] tokens = time.split(TIME_DELIMETER);
        return Integer.parseInt(tokens[0]);
    }

    private int extractGetOrPost(String data) {
        Pattern pattern = Pattern.compile(GET_REQUEST_REGEX+POST_REQUEST_REGEX);
        Matcher matcher = pattern.matcher(data);
        String getOrPost = "";
        while (matcher.find()) {
            getOrPost = matcher.group();
        }
        if (getOrPost.equals(GET_REQUEST)) {
            return 1;
        } else if (getOrPost.equals(POST_REQUEST)) {
            return 0;
        } else {
            return -1;
        }
    }

    private String extractUri(String data) {
        Pattern pattern = Pattern.compile(URI_REGEX);
        Matcher matcher = pattern.matcher(data);
        String uri = "";
        while (matcher.find()) {
            uri = matcher.group();
        }
        return uri;
    }

    private int extractResponseTime(String data) {
        Pattern pattern = Pattern.compile(RESPONSE_TIME_REGEX);
        Matcher matcher = pattern.matcher(data);
        String responseTime = "";
        while (matcher.find()) {
            responseTime = matcher.group();
        }
        String[] tokens = responseTime.split(RESPONSE_TIME_DELIMETER);
        return Integer.parseInt(tokens[1]);
    }

    public void sortLogData() {
        for (int i = 0; i < TOTAL_HOURS; ++i) {
            if (logData[i].getHour() == UNSET_VALUE) {
                logData[i].setHour(i);
            }
        }
        Arrays.sort(logData);
    }

    public String[] getSummary() {
        String[] summary = new String[TOTAL_HOURS];
        for (int i = 0; i < TOTAL_HOURS; ++i) {
            summary[i] = "";
            int hourValue = logData[i].getHour();
            int totalGetRequest = logData[i].getTotalGetRequest();
            int totalPostRequest = logData[i].getTotalPostRequest();
            int totalUniqueUri = logData[i].getTotalUri();
            int totalResponseTime = logData[i].getTotalResponseTime();
            if (hourValue == 0) {
                summary[i] += (hourValue + 12) + ".00 am - " + (hourValue + 1) + ".00 am   ";
            } else if (hourValue < 11) {
                summary[i] += hourValue + ".00 am - " + (hourValue + 1) + ".00 am   ";
            } else if (hourValue == 11) {
                summary[i] += hourValue + ".00 am - " + (hourValue + 1) + ".00 pm   ";
            } else if (hourValue == 12) {
                summary[i] += hourValue + ".00 pm - " + (hourValue - 11) + ".00 pm   ";
            } else if (hourValue < 23) {
                summary[i] += (hourValue - 12) + ".00 pm - " + (hourValue - 11) + ".00 pm   ";
            } else {
                summary[i] += (hourValue - 12) + ".00 pm - " + (hourValue - 11) + ".00 am   ";
            }
            summary[i] += totalGetRequest + "/" + totalPostRequest + "   ";
            summary[i] += totalUniqueUri + "   ";
            summary[i] += totalResponseTime;
        }
        return summary;
    }
}
