package net.therap.view;

/**
 * @author al.imran
 * @since 10/03/2021
 */
public class LogParserView {
    private static final int TOTAL_HOURS = 24;

    public static void viewLogParserData(String[] summary) {
        for (int i = 0; i < TOTAL_HOURS; ++i) {
            System.out.println(summary[i]);
        }
    }
}
