package net.therap;

import net.therap.controller.LogParserController;
import net.therap.service.LogParserService;

/**
 * @author al.imran
 * @since 09/03/2021
 */
public class LogParser {
    public static void main(String[] args) {
        boolean isSort = false;
        if (args.length >= 2 && args[1].equals("--sort")) {
            isSort = true;
        }
        if (args.length >= 1) {
            LogParserService logParserService = new LogParserService();
            LogParserController logParserController = new LogParserController(logParserService);
            logParserController.readLogParserData(args[0]);
            if (isSort) {
                logParserController.sortLogData();
            }
            logParserController.updateView();
        } else {
            System.out.println("File has not been provided");
        }
    }
}
