package net.therap.model;

import java.util.*;

/**
 * @author al.imran
 * @since 10/03/2021
 */
public class LogParserModel implements Comparable<LogParserModel>{
    private static final int INVALID_HOUR = -1;
    private int hour;
    private int totalGetRequest;
    private int totalPostRequest;
    private int totalResponseTime;
    private Set<String> uri;

    public LogParserModel() {
        this.hour = INVALID_HOUR;
        this.uri = new HashSet<>();
    }

    public void addUri(String uri) {
        this.uri.add(uri);
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void addTotalGetRequest() {
        this.totalGetRequest += 1;
    }

    public void addTotalPostRequest() {
        this.totalPostRequest += 1;
    }

    public void addTotalResponseTime(int time) {
        this.totalResponseTime += time;
    }

    public int getTotalUri() {
        return uri.size();
    }

    public int getHour() {
        return hour;
    }

    public int getTotalGetRequest() {
        return totalGetRequest;
    }

    public int getTotalPostRequest() {
        return totalPostRequest;
    }

    public int getTotalResponseTime() {
        return totalResponseTime;
    }

    public int getTotalRequest() {
        return (totalGetRequest + totalPostRequest);
    }

    @Override
    public int compareTo(LogParserModel logParserModel) {
        if ((this.getTotalRequest()) < (logParserModel.getTotalRequest())) {
            return -1;
        } else if ((this.getTotalRequest()) > (logParserModel.getTotalRequest())) {
            return 1;
        } else {
            return 0;
        }
    }
}
