package net.therap.controller;

import net.therap.service.LogParserService;
import net.therap.view.LogParserView;

/**
 * @author al.imran
 * @since 09/03/2021
 */
public class LogParserController {
    private final LogParserService logParserService;

    public LogParserController(LogParserService logParserService) {
        this.logParserService = logParserService;
    }

    public void readLogParserData(String fileName) {
        logParserService.readLogParserData(fileName);
    }

    public void sortLogData() {
        logParserService.sortLogData();
    }

    public void updateView() {
        LogParserView.viewLogParserData(logParserService.getSummary());
    }
}
